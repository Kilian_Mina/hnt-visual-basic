﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pays_Modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Pays_Modifier))
        Me.lpaysmodif = New System.Windows.Forms.Label()
        Me.lnom = New System.Windows.Forms.Label()
        Me.lrisque = New System.Windows.Forms.Label()
        Me.tbnom = New System.Windows.Forms.TextBox()
        Me.rbaucun = New System.Windows.Forms.RadioButton()
        Me.rbfaible = New System.Windows.Forms.RadioButton()
        Me.rbmoyen = New System.Windows.Forms.RadioButton()
        Me.rbeleve = New System.Windows.Forms.RadioButton()
        Me.rbtreseleve = New System.Windows.Forms.RadioButton()
        Me.bmodif = New System.Windows.Forms.Button()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'lpaysmodif
        '
        Me.lpaysmodif.AutoSize = True
        Me.lpaysmodif.BackColor = System.Drawing.Color.White
        Me.lpaysmodif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lpaysmodif.Location = New System.Drawing.Point(12, 9)
        Me.lpaysmodif.Name = "lpaysmodif"
        Me.lpaysmodif.Size = New System.Drawing.Size(215, 31)
        Me.lpaysmodif.TabIndex = 0
        Me.lpaysmodif.Text = "Modifier un Pays"
        '
        'lnom
        '
        Me.lnom.AutoSize = True
        Me.lnom.BackColor = System.Drawing.Color.White
        Me.lnom.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnom.Location = New System.Drawing.Point(12, 86)
        Me.lnom.Name = "lnom"
        Me.lnom.Size = New System.Drawing.Size(45, 17)
        Me.lnom.TabIndex = 1
        Me.lnom.Text = "Nom: "
        '
        'lrisque
        '
        Me.lrisque.AutoSize = True
        Me.lrisque.BackColor = System.Drawing.Color.White
        Me.lrisque.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lrisque.Location = New System.Drawing.Point(12, 124)
        Me.lrisque.Name = "lrisque"
        Me.lrisque.Size = New System.Drawing.Size(60, 17)
        Me.lrisque.TabIndex = 2
        Me.lrisque.Text = "Risque :"
        '
        'tbnom
        '
        Me.tbnom.Location = New System.Drawing.Point(101, 88)
        Me.tbnom.Name = "tbnom"
        Me.tbnom.Size = New System.Drawing.Size(100, 20)
        Me.tbnom.TabIndex = 3
        '
        'rbaucun
        '
        Me.rbaucun.AutoSize = True
        Me.rbaucun.BackColor = System.Drawing.Color.White
        Me.rbaucun.Location = New System.Drawing.Point(101, 126)
        Me.rbaucun.Name = "rbaucun"
        Me.rbaucun.Size = New System.Drawing.Size(56, 17)
        Me.rbaucun.TabIndex = 4
        Me.rbaucun.TabStop = True
        Me.rbaucun.Text = "Aucun"
        Me.rbaucun.UseVisualStyleBackColor = False
        '
        'rbfaible
        '
        Me.rbfaible.AutoSize = True
        Me.rbfaible.BackColor = System.Drawing.Color.White
        Me.rbfaible.Location = New System.Drawing.Point(101, 149)
        Me.rbfaible.Name = "rbfaible"
        Me.rbfaible.Size = New System.Drawing.Size(53, 17)
        Me.rbfaible.TabIndex = 5
        Me.rbfaible.TabStop = True
        Me.rbfaible.Text = "Faible"
        Me.rbfaible.UseVisualStyleBackColor = False
        '
        'rbmoyen
        '
        Me.rbmoyen.AutoSize = True
        Me.rbmoyen.BackColor = System.Drawing.Color.White
        Me.rbmoyen.Location = New System.Drawing.Point(101, 172)
        Me.rbmoyen.Name = "rbmoyen"
        Me.rbmoyen.Size = New System.Drawing.Size(57, 17)
        Me.rbmoyen.TabIndex = 6
        Me.rbmoyen.TabStop = True
        Me.rbmoyen.Text = "Moyen"
        Me.rbmoyen.UseVisualStyleBackColor = False
        '
        'rbeleve
        '
        Me.rbeleve.AutoSize = True
        Me.rbeleve.BackColor = System.Drawing.Color.White
        Me.rbeleve.Location = New System.Drawing.Point(101, 195)
        Me.rbeleve.Name = "rbeleve"
        Me.rbeleve.Size = New System.Drawing.Size(52, 17)
        Me.rbeleve.TabIndex = 7
        Me.rbeleve.TabStop = True
        Me.rbeleve.Text = "Elevé"
        Me.rbeleve.UseVisualStyleBackColor = False
        '
        'rbtreseleve
        '
        Me.rbtreseleve.AutoSize = True
        Me.rbtreseleve.BackColor = System.Drawing.Color.White
        Me.rbtreseleve.Location = New System.Drawing.Point(101, 218)
        Me.rbtreseleve.Name = "rbtreseleve"
        Me.rbtreseleve.Size = New System.Drawing.Size(75, 17)
        Me.rbtreseleve.TabIndex = 8
        Me.rbtreseleve.TabStop = True
        Me.rbtreseleve.Text = "Très élevé"
        Me.rbtreseleve.UseVisualStyleBackColor = False
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(15, 261)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(118, 30)
        Me.bmodif.TabIndex = 9
        Me.bmodif.Text = "Modifier"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(101, 62)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 23
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(12, 63)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 22
        Me.lid.Text = "ID : "
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(174, 279)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Pays_Modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(261, 314)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.rbtreseleve)
        Me.Controls.Add(Me.rbeleve)
        Me.Controls.Add(Me.rbmoyen)
        Me.Controls.Add(Me.rbfaible)
        Me.Controls.Add(Me.rbaucun)
        Me.Controls.Add(Me.tbnom)
        Me.Controls.Add(Me.lrisque)
        Me.Controls.Add(Me.lnom)
        Me.Controls.Add(Me.lpaysmodif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Pays_Modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pays_Modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lpaysmodif As System.Windows.Forms.Label
    Friend WithEvents lnom As System.Windows.Forms.Label
    Friend WithEvents lrisque As System.Windows.Forms.Label
    Friend WithEvents tbnom As System.Windows.Forms.TextBox
    Friend WithEvents rbaucun As System.Windows.Forms.RadioButton
    Friend WithEvents rbfaible As System.Windows.Forms.RadioButton
    Friend WithEvents rbmoyen As System.Windows.Forms.RadioButton
    Friend WithEvents rbeleve As System.Windows.Forms.RadioButton
    Friend WithEvents rbtreseleve As System.Windows.Forms.RadioButton
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class

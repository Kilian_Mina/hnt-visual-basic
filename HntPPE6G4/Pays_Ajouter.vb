﻿Public Class Pays_Ajouter
    Dim maconnexion As New Connexion
    Dim risque As String
    Private Sub Pays_Ajouter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tbnom.Text = ""
    End Sub
    Private Sub bajout_Click(sender As Object, e As EventArgs) Handles bajout.Click
        Dim nom As String
        nom = tbnom.Text
        maconnexion.ajouter_pays(nom:=nom, risque:=risque)
        Me.Close()
        Pays.Show()
    End Sub

    Private Sub rbaucun_CheckedChanged(sender As Object, e As EventArgs) Handles rbaucun.CheckedChanged
        If rbaucun.Checked = True Then
            risque = rbaucun.Text
        End If
    End Sub

    Private Sub rbfaible_CheckedChanged(sender As Object, e As EventArgs) Handles rbfaible.CheckedChanged
        If rbfaible.Checked = True Then
            risque = rbfaible.Text
        End If
    End Sub

    Private Sub rbmoyen_CheckedChanged(sender As Object, e As EventArgs) Handles rbmoyen.CheckedChanged
        If rbmoyen.Checked = True Then
            risque = rbmoyen.Text
        End If
    End Sub

    Private Sub rbeleve_CheckedChanged(sender As Object, e As EventArgs) Handles rbeleve.CheckedChanged
        If rbeleve.Checked = True Then
            risque = rbeleve.Text
        End If
    End Sub

    Private Sub rbtreseleve_CheckedChanged(sender As Object, e As EventArgs) Handles rbtreseleve.CheckedChanged
        If rbtreseleve.Checked = True Then
            risque = rbtreseleve.Text
        End If
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Pays.Show()
    End Sub
End Class
﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mission_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mission_Ajouter))
        Me.bajout = New System.Windows.Forms.Button()
        Me.datefin = New System.Windows.Forms.DateTimePicker()
        Me.datedeb = New System.Windows.Forms.DateTimePicker()
        Me.tbdescrip = New System.Windows.Forms.TextBox()
        Me.tbnommission = New System.Windows.Forms.TextBox()
        Me.ldatefin = New System.Windows.Forms.Label()
        Me.ldatedeb = New System.Windows.Forms.Label()
        Me.ldescript = New System.Windows.Forms.Label()
        Me.lnommission = New System.Windows.Forms.Label()
        Me.lnompays = New System.Windows.Forms.Label()
        Me.lmission_ajout = New System.Windows.Forms.Label()
        Me.datecreat = New System.Windows.Forms.DateTimePicker()
        Me.ldatecrea = New System.Windows.Forms.Label()
        Me.cbnompays = New System.Windows.Forms.ComboBox()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(169, 310)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(88, 35)
        Me.bajout.TabIndex = 23
        Me.bajout.Text = "AJOUTER"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'datefin
        '
        Me.datefin.Location = New System.Drawing.Point(123, 263)
        Me.datefin.Name = "datefin"
        Me.datefin.Size = New System.Drawing.Size(200, 20)
        Me.datefin.TabIndex = 22
        '
        'datedeb
        '
        Me.datedeb.Location = New System.Drawing.Point(123, 231)
        Me.datedeb.Name = "datedeb"
        Me.datedeb.Size = New System.Drawing.Size(200, 20)
        Me.datedeb.TabIndex = 21
        '
        'tbdescrip
        '
        Me.tbdescrip.Location = New System.Drawing.Point(123, 163)
        Me.tbdescrip.Name = "tbdescrip"
        Me.tbdescrip.Size = New System.Drawing.Size(100, 20)
        Me.tbdescrip.TabIndex = 20
        '
        'tbnommission
        '
        Me.tbnommission.Location = New System.Drawing.Point(123, 127)
        Me.tbnommission.Name = "tbnommission"
        Me.tbnommission.Size = New System.Drawing.Size(100, 20)
        Me.tbnommission.TabIndex = 19
        '
        'ldatefin
        '
        Me.ldatefin.AutoSize = True
        Me.ldatefin.BackColor = System.Drawing.Color.White
        Me.ldatefin.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatefin.Location = New System.Drawing.Point(12, 267)
        Me.ldatefin.Name = "ldatefin"
        Me.ldatefin.Size = New System.Drawing.Size(61, 17)
        Me.ldatefin.TabIndex = 17
        Me.ldatefin.Text = "Date fin:"
        '
        'ldatedeb
        '
        Me.ldatedeb.AutoSize = True
        Me.ldatedeb.BackColor = System.Drawing.Color.White
        Me.ldatedeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatedeb.Location = New System.Drawing.Point(13, 235)
        Me.ldatedeb.Name = "ldatedeb"
        Me.ldatedeb.Size = New System.Drawing.Size(82, 17)
        Me.ldatedeb.TabIndex = 16
        Me.ldatedeb.Text = "Date début:"
        '
        'ldescript
        '
        Me.ldescript.AutoSize = True
        Me.ldescript.BackColor = System.Drawing.Color.White
        Me.ldescript.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldescript.Location = New System.Drawing.Point(12, 163)
        Me.ldescript.Name = "ldescript"
        Me.ldescript.Size = New System.Drawing.Size(83, 17)
        Me.ldescript.TabIndex = 15
        Me.ldescript.Text = "Description:"
        '
        'lnommission
        '
        Me.lnommission.AutoSize = True
        Me.lnommission.BackColor = System.Drawing.Color.White
        Me.lnommission.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommission.Location = New System.Drawing.Point(12, 130)
        Me.lnommission.Name = "lnommission"
        Me.lnommission.Size = New System.Drawing.Size(92, 17)
        Me.lnommission.TabIndex = 14
        Me.lnommission.Text = "Nom mission:"
        '
        'lnompays
        '
        Me.lnompays.AutoSize = True
        Me.lnompays.BackColor = System.Drawing.Color.White
        Me.lnompays.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnompays.Location = New System.Drawing.Point(13, 93)
        Me.lnompays.Name = "lnompays"
        Me.lnompays.Size = New System.Drawing.Size(75, 17)
        Me.lnompays.TabIndex = 13
        Me.lnompays.Text = "Nom pays:"
        '
        'lmission_ajout
        '
        Me.lmission_ajout.AutoSize = True
        Me.lmission_ajout.BackColor = System.Drawing.Color.White
        Me.lmission_ajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmission_ajout.Location = New System.Drawing.Point(12, 9)
        Me.lmission_ajout.Name = "lmission_ajout"
        Me.lmission_ajout.Size = New System.Drawing.Size(251, 31)
        Me.lmission_ajout.TabIndex = 12
        Me.lmission_ajout.Text = "Ajouter une mission"
        '
        'datecreat
        '
        Me.datecreat.Location = New System.Drawing.Point(123, 196)
        Me.datecreat.Name = "datecreat"
        Me.datecreat.Size = New System.Drawing.Size(200, 20)
        Me.datecreat.TabIndex = 25
        '
        'ldatecrea
        '
        Me.ldatecrea.AutoSize = True
        Me.ldatecrea.BackColor = System.Drawing.Color.White
        Me.ldatecrea.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldatecrea.Location = New System.Drawing.Point(12, 200)
        Me.ldatecrea.Name = "ldatecrea"
        Me.ldatecrea.Size = New System.Drawing.Size(97, 17)
        Me.ldatecrea.TabIndex = 24
        Me.ldatecrea.Text = "Date création:"
        '
        'cbnompays
        '
        Me.cbnompays.FormattingEnabled = True
        Me.cbnompays.Location = New System.Drawing.Point(123, 89)
        Me.cbnompays.Name = "cbnompays"
        Me.cbnompays.Size = New System.Drawing.Size(121, 21)
        Me.cbnompays.TabIndex = 28
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(339, 322)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Mission_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(426, 357)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.cbnompays)
        Me.Controls.Add(Me.datecreat)
        Me.Controls.Add(Me.ldatecrea)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.datefin)
        Me.Controls.Add(Me.datedeb)
        Me.Controls.Add(Me.tbdescrip)
        Me.Controls.Add(Me.tbnommission)
        Me.Controls.Add(Me.ldatefin)
        Me.Controls.Add(Me.ldatedeb)
        Me.Controls.Add(Me.ldescript)
        Me.Controls.Add(Me.lnommission)
        Me.Controls.Add(Me.lnompays)
        Me.Controls.Add(Me.lmission_ajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Mission_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Mission_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents datefin As System.Windows.Forms.DateTimePicker
    Friend WithEvents datedeb As System.Windows.Forms.DateTimePicker
    Friend WithEvents tbdescrip As System.Windows.Forms.TextBox
    Friend WithEvents tbnommission As System.Windows.Forms.TextBox
    Friend WithEvents ldatefin As System.Windows.Forms.Label
    Friend WithEvents ldatedeb As System.Windows.Forms.Label
    Friend WithEvents ldescript As System.Windows.Forms.Label
    Friend WithEvents lnommission As System.Windows.Forms.Label
    Friend WithEvents lnompays As System.Windows.Forms.Label
    Friend WithEvents lmission_ajout As System.Windows.Forms.Label
    Friend WithEvents datecreat As System.Windows.Forms.DateTimePicker
    Friend WithEvents ldatecrea As System.Windows.Forms.Label
    Friend WithEvents cbnompays As System.Windows.Forms.ComboBox
    Friend WithEvents bretour As Button
End Class

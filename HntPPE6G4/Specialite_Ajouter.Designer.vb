﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Specialite_Ajouter
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Specialite_Ajouter))
        Me.tblibspec = New System.Windows.Forms.TextBox()
        Me.llibspec = New System.Windows.Forms.Label()
        Me.bajout = New System.Windows.Forms.Button()
        Me.lspecajout = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'tblibspec
        '
        Me.tblibspec.Location = New System.Drawing.Point(130, 76)
        Me.tblibspec.Name = "tblibspec"
        Me.tblibspec.Size = New System.Drawing.Size(100, 20)
        Me.tblibspec.TabIndex = 39
        '
        'llibspec
        '
        Me.llibspec.AutoSize = True
        Me.llibspec.BackColor = System.Drawing.Color.White
        Me.llibspec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llibspec.Location = New System.Drawing.Point(12, 77)
        Me.llibspec.Name = "llibspec"
        Me.llibspec.Size = New System.Drawing.Size(57, 17)
        Me.llibspec.TabIndex = 38
        Me.llibspec.Text = "Libellé :"
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(28, 123)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(118, 30)
        Me.bajout.TabIndex = 37
        Me.bajout.Text = "Ajouter"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'lspecajout
        '
        Me.lspecajout.AutoSize = True
        Me.lspecajout.BackColor = System.Drawing.Color.White
        Me.lspecajout.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lspecajout.Location = New System.Drawing.Point(12, 9)
        Me.lspecajout.Name = "lspecajout"
        Me.lspecajout.Size = New System.Drawing.Size(262, 31)
        Me.lspecajout.TabIndex = 34
        Me.lspecajout.Text = "Ajouter un Spécialité"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(200, 130)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Specialite_Ajouter
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(287, 165)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.tblibspec)
        Me.Controls.Add(Me.llibspec)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.lspecajout)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Specialite_Ajouter"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Specialite_Ajouter"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tblibspec As System.Windows.Forms.TextBox
    Friend WithEvents llibspec As System.Windows.Forms.Label
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents lspecajout As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class

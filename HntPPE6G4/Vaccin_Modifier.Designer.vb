﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Vaccin_Modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Vaccin_Modifier))
        Me.bmodif = New System.Windows.Forms.Button()
        Me.tbdescrip = New System.Windows.Forms.TextBox()
        Me.ldescrip = New System.Windows.Forms.Label()
        Me.lvaccinmodif = New System.Windows.Forms.Label()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(15, 155)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(118, 30)
        Me.bmodif.TabIndex = 19
        Me.bmodif.Text = "Modifier"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'tbdescrip
        '
        Me.tbdescrip.Location = New System.Drawing.Point(124, 107)
        Me.tbdescrip.Name = "tbdescrip"
        Me.tbdescrip.Size = New System.Drawing.Size(100, 20)
        Me.tbdescrip.TabIndex = 13
        '
        'ldescrip
        '
        Me.ldescrip.AutoSize = True
        Me.ldescrip.BackColor = System.Drawing.Color.White
        Me.ldescrip.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldescrip.Location = New System.Drawing.Point(15, 108)
        Me.ldescrip.Name = "ldescrip"
        Me.ldescrip.Size = New System.Drawing.Size(87, 17)
        Me.ldescrip.TabIndex = 11
        Me.ldescrip.Text = "Description :"
        '
        'lvaccinmodif
        '
        Me.lvaccinmodif.AutoSize = True
        Me.lvaccinmodif.BackColor = System.Drawing.Color.White
        Me.lvaccinmodif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lvaccinmodif.Location = New System.Drawing.Point(12, 9)
        Me.lvaccinmodif.Name = "lvaccinmodif"
        Me.lvaccinmodif.Size = New System.Drawing.Size(236, 31)
        Me.lvaccinmodif.TabIndex = 10
        Me.lvaccinmodif.Text = "Modifier un Vaccin"
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(124, 65)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 21
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(15, 66)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 20
        Me.lid.Text = "ID : "
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(183, 167)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Vaccin_Modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(270, 202)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.tbdescrip)
        Me.Controls.Add(Me.ldescrip)
        Me.Controls.Add(Me.lvaccinmodif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Vaccin_Modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Vaccin_Modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents tbdescrip As System.Windows.Forms.TextBox
    Friend WithEvents ldescrip As System.Windows.Forms.Label
    Friend WithEvents lvaccinmodif As System.Windows.Forms.Label
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class

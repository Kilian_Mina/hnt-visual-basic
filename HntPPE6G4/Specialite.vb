﻿Public Class Specialite
    Dim maconnexion As New Connexion
    Private Sub Specialite_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_spec()
        bmodifier.Visible = False
        bsupprimer.Visible = False
    End Sub
    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvspec
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Specialite_Modifier.tbid.Text = IdLigne
            Specialite_Modifier.tblibspec.Text = ValLigne
        End With
        Me.Close()
        Specialite_Modifier.Show()
    End Sub

    Private Sub bnouvelle_Click(sender As Object, e As EventArgs) Handles bnouvelle.Click
        Me.Close()
        Specialite_Ajouter.Show()
    End Sub

    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub
    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvspec
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer la spécialité : " + IdLigne + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_specialite(ValLigne)
            End If
        End With
        Me.Close()
        Specialite_Chargement.Show()
    End Sub

    Private Sub dgvspec_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvspec.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
    End Sub
End Class
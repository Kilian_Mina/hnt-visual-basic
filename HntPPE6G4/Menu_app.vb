﻿Public Class Menu_app
    Dim maconnexion As New Connexion
    Private Sub bquitter_Click(sender As Object, e As EventArgs) Handles bquitter.Click
        If MsgBox("Quitter l'application ?", vbQuestion + vbYesNo, "Quitter") = vbYes Then
            maconnexion.envoyer_collection_bdd()
            Me.Close()
            Login_form.Close()
        End If
    End Sub

    Private Sub bmission_Click(sender As Object, e As EventArgs) Handles bmission.Click
        Me.Hide()
        Mission.Show()
    End Sub

    Private Sub bmembres_Click(sender As Object, e As EventArgs) Handles bmembres.Click
        Me.Hide()
        Membre.Show()
    End Sub

    Private Sub bdons_Click(sender As Object, e As EventArgs) Handles bdons.Click
        Me.Hide()
        Don.Show()
    End Sub

    Private Sub Menu_app_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        bpays.Visible = False
        bvaccin.Visible = False
        brole.Visible = False
        bspec.Visible = False
    End Sub

    Private Sub bparam_Click(sender As Object, e As EventArgs) Handles bparam.Click
        bpays.Visible = True
        bvaccin.Visible = True
        brole.Visible = True
        bspec.Visible = True
    End Sub

    Private Sub bpays_Click(sender As Object, e As EventArgs) Handles bpays.Click
        Me.Hide()
        Pays.Show()
    End Sub

    Private Sub bvaccin_Click(sender As Object, e As EventArgs) Handles bvaccin.Click
        Me.Hide()
        Vaccin.Show()
    End Sub

    Private Sub brole_Click(sender As Object, e As EventArgs) Handles brole.Click
        Me.Hide()
        Role.Show()
    End Sub

    Private Sub bspec_Click(sender As Object, e As EventArgs) Handles bspec.Click
        Me.Hide()
        Specialite.Show()
    End Sub

    Private Sub bdeco_Click(sender As Object, e As EventArgs) Handles bdeco.Click
        Me.Hide()
        Login_form.Show()
    End Sub
End Class
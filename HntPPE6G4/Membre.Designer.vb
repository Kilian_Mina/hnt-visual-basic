﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Membre
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Membre))
        Me.bnouveau = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.lmembre = New System.Windows.Forms.Label()
        Me.dgvdonateur = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prénom = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Adresse = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GSM = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        Me.dgvbenevole = New System.Windows.Forms.DataGridView()
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DataGridViewTextBoxColumn5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dateEntree = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dateSortie = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ldonateur = New System.Windows.Forms.Label()
        Me.lbenevole = New System.Windows.Forms.Label()
        CType(Me.dgvdonateur, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvbenevole, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouveau
        '
        Me.bnouveau.BackColor = System.Drawing.Color.White
        Me.bnouveau.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouveau.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouveau.Location = New System.Drawing.Point(18, 363)
        Me.bnouveau.Name = "bnouveau"
        Me.bnouveau.Size = New System.Drawing.Size(220, 58)
        Me.bnouveau.TabIndex = 9
        Me.bnouveau.Text = "NOUVEAU MEMBRE"
        Me.bnouveau.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(621, 363)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(141, 58)
        Me.bsupprimer.TabIndex = 8
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(432, 363)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(132, 58)
        Me.bmodifier.TabIndex = 7
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'lmembre
        '
        Me.lmembre.AutoSize = True
        Me.lmembre.BackColor = System.Drawing.Color.White
        Me.lmembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmembre.Location = New System.Drawing.Point(12, 9)
        Me.lmembre.Name = "lmembre"
        Me.lmembre.Size = New System.Drawing.Size(150, 31)
        Me.lmembre.TabIndex = 6
        Me.lmembre.Text = "MEMBRES"
        '
        'dgvdonateur
        '
        Me.dgvdonateur.AllowUserToAddRows = False
        Me.dgvdonateur.AllowUserToDeleteRows = False
        Me.dgvdonateur.AllowUserToResizeColumns = False
        Me.dgvdonateur.AllowUserToResizeRows = False
        Me.dgvdonateur.BackgroundColor = System.Drawing.Color.White
        Me.dgvdonateur.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdonateur.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.Nom, Me.Prénom, Me.Adresse, Me.GSM})
        Me.dgvdonateur.Location = New System.Drawing.Point(12, 129)
        Me.dgvdonateur.Name = "dgvdonateur"
        Me.dgvdonateur.Size = New System.Drawing.Size(540, 184)
        Me.dgvdonateur.TabIndex = 5
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'Nom
        '
        Me.Nom.Frozen = True
        Me.Nom.HeaderText = "Nom"
        Me.Nom.Name = "Nom"
        Me.Nom.ReadOnly = True
        '
        'Prénom
        '
        Me.Prénom.Frozen = True
        Me.Prénom.HeaderText = "Prénom"
        Me.Prénom.Name = "Prénom"
        Me.Prénom.ReadOnly = True
        '
        'Adresse
        '
        Me.Adresse.Frozen = True
        Me.Adresse.HeaderText = "Adresse"
        Me.Adresse.Name = "Adresse"
        Me.Adresse.ReadOnly = True
        '
        'GSM
        '
        Me.GSM.Frozen = True
        Me.GSM.HeaderText = "GSM"
        Me.GSM.Name = "GSM"
        Me.GSM.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(1207, 397)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(151, 47)
        Me.bmenu.TabIndex = 10
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'dgvbenevole
        '
        Me.dgvbenevole.AllowUserToAddRows = False
        Me.dgvbenevole.AllowUserToDeleteRows = False
        Me.dgvbenevole.AllowUserToResizeColumns = False
        Me.dgvbenevole.AllowUserToResizeRows = False
        Me.dgvbenevole.BackgroundColor = System.Drawing.Color.White
        Me.dgvbenevole.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvbenevole.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3, Me.DataGridViewTextBoxColumn4, Me.DataGridViewTextBoxColumn5, Me.dateEntree, Me.dateSortie})
        Me.dgvbenevole.Location = New System.Drawing.Point(603, 129)
        Me.dgvbenevole.Name = "dgvbenevole"
        Me.dgvbenevole.Size = New System.Drawing.Size(741, 184)
        Me.dgvbenevole.TabIndex = 11
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.Frozen = True
        Me.DataGridViewTextBoxColumn1.HeaderText = "ID"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.Frozen = True
        Me.DataGridViewTextBoxColumn2.HeaderText = "Nom"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.Frozen = True
        Me.DataGridViewTextBoxColumn3.HeaderText = "Prénom"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        '
        'DataGridViewTextBoxColumn4
        '
        Me.DataGridViewTextBoxColumn4.Frozen = True
        Me.DataGridViewTextBoxColumn4.HeaderText = "Adresse"
        Me.DataGridViewTextBoxColumn4.Name = "DataGridViewTextBoxColumn4"
        Me.DataGridViewTextBoxColumn4.ReadOnly = True
        '
        'DataGridViewTextBoxColumn5
        '
        Me.DataGridViewTextBoxColumn5.Frozen = True
        Me.DataGridViewTextBoxColumn5.HeaderText = "GSM"
        Me.DataGridViewTextBoxColumn5.Name = "DataGridViewTextBoxColumn5"
        Me.DataGridViewTextBoxColumn5.ReadOnly = True
        '
        'dateEntree
        '
        Me.dateEntree.Frozen = True
        Me.dateEntree.HeaderText = "Date d'entrée"
        Me.dateEntree.Name = "dateEntree"
        Me.dateEntree.ReadOnly = True
        '
        'dateSortie
        '
        Me.dateSortie.Frozen = True
        Me.dateSortie.HeaderText = "Date de sortie"
        Me.dateSortie.Name = "dateSortie"
        Me.dateSortie.ReadOnly = True
        '
        'ldonateur
        '
        Me.ldonateur.AutoSize = True
        Me.ldonateur.BackColor = System.Drawing.Color.White
        Me.ldonateur.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldonateur.Location = New System.Drawing.Point(13, 82)
        Me.ldonateur.Name = "ldonateur"
        Me.ldonateur.Size = New System.Drawing.Size(92, 25)
        Me.ldonateur.TabIndex = 12
        Me.ldonateur.Text = "Donateur"
        '
        'lbenevole
        '
        Me.lbenevole.AutoSize = True
        Me.lbenevole.BackColor = System.Drawing.Color.White
        Me.lbenevole.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbenevole.Location = New System.Drawing.Point(616, 82)
        Me.lbenevole.Name = "lbenevole"
        Me.lbenevole.Size = New System.Drawing.Size(94, 25)
        Me.lbenevole.TabIndex = 13
        Me.lbenevole.Text = "Bénévole"
        '
        'Membre
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(1370, 456)
        Me.Controls.Add(Me.lbenevole)
        Me.Controls.Add(Me.ldonateur)
        Me.Controls.Add(Me.dgvbenevole)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.bnouveau)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lmembre)
        Me.Controls.Add(Me.dgvdonateur)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Membre"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Membre"
        CType(Me.dgvdonateur, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvbenevole, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouveau As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents lmembre As System.Windows.Forms.Label
    Friend WithEvents dgvdonateur As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents dgvbenevole As System.Windows.Forms.DataGridView
    Friend WithEvents ldonateur As System.Windows.Forms.Label
    Friend WithEvents lbenevole As System.Windows.Forms.Label
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prénom As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Adresse As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents GSM As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dateEntree As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents dateSortie As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

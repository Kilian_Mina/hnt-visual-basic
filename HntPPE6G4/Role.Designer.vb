﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Role
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Role))
        Me.bnouveau = New System.Windows.Forms.Button()
        Me.bsupprimer = New System.Windows.Forms.Button()
        Me.bmodifier = New System.Windows.Forms.Button()
        Me.lrole = New System.Windows.Forms.Label()
        Me.dgvrole = New System.Windows.Forms.DataGridView()
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodeSpec = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LibR = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        CType(Me.dgvrole, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouveau
        '
        Me.bnouveau.BackColor = System.Drawing.Color.White
        Me.bnouveau.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouveau.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouveau.Location = New System.Drawing.Point(361, 62)
        Me.bnouveau.Name = "bnouveau"
        Me.bnouveau.Size = New System.Drawing.Size(178, 58)
        Me.bnouveau.TabIndex = 19
        Me.bnouveau.Text = "NOUVEAU RÔLE"
        Me.bnouveau.UseVisualStyleBackColor = False
        '
        'bsupprimer
        '
        Me.bsupprimer.BackColor = System.Drawing.Color.White
        Me.bsupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bsupprimer.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bsupprimer.Location = New System.Drawing.Point(361, 190)
        Me.bsupprimer.Name = "bsupprimer"
        Me.bsupprimer.Size = New System.Drawing.Size(178, 58)
        Me.bsupprimer.TabIndex = 18
        Me.bsupprimer.Text = "SUPPRIMER"
        Me.bsupprimer.UseVisualStyleBackColor = False
        '
        'bmodifier
        '
        Me.bmodifier.BackColor = System.Drawing.Color.White
        Me.bmodifier.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodifier.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmodifier.Location = New System.Drawing.Point(361, 126)
        Me.bmodifier.Name = "bmodifier"
        Me.bmodifier.Size = New System.Drawing.Size(178, 58)
        Me.bmodifier.TabIndex = 17
        Me.bmodifier.Text = "MODIFIER"
        Me.bmodifier.UseVisualStyleBackColor = False
        '
        'lrole
        '
        Me.lrole.AutoSize = True
        Me.lrole.BackColor = System.Drawing.Color.White
        Me.lrole.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lrole.Location = New System.Drawing.Point(12, 9)
        Me.lrole.Name = "lrole"
        Me.lrole.Size = New System.Drawing.Size(88, 31)
        Me.lrole.TabIndex = 16
        Me.lrole.Text = "RÔLE"
        '
        'dgvrole
        '
        Me.dgvrole.AllowUserToAddRows = False
        Me.dgvrole.AllowUserToDeleteRows = False
        Me.dgvrole.AllowUserToResizeColumns = False
        Me.dgvrole.AllowUserToResizeRows = False
        Me.dgvrole.BackgroundColor = System.Drawing.Color.White
        Me.dgvrole.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvrole.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.CodeSpec, Me.LibR})
        Me.dgvrole.Location = New System.Drawing.Point(12, 62)
        Me.dgvrole.Name = "dgvrole"
        Me.dgvrole.Size = New System.Drawing.Size(343, 184)
        Me.dgvrole.TabIndex = 15
        '
        'ID
        '
        Me.ID.Frozen = True
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        '
        'CodeSpec
        '
        Me.CodeSpec.Frozen = True
        Me.CodeSpec.HeaderText = "CodeSpec"
        Me.CodeSpec.Name = "CodeSpec"
        Me.CodeSpec.ReadOnly = True
        '
        'LibR
        '
        Me.LibR.Frozen = True
        Me.LibR.HeaderText = "Role"
        Me.LibR.Name = "LibR"
        Me.LibR.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(490, 254)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(92, 58)
        Me.bmenu.TabIndex = 20
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'Role
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(594, 323)
        Me.Controls.Add(Me.bmenu)
        Me.Controls.Add(Me.bnouveau)
        Me.Controls.Add(Me.bsupprimer)
        Me.Controls.Add(Me.bmodifier)
        Me.Controls.Add(Me.lrole)
        Me.Controls.Add(Me.dgvrole)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Role"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Role"
        CType(Me.dgvrole, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouveau As System.Windows.Forms.Button
    Friend WithEvents bsupprimer As System.Windows.Forms.Button
    Friend WithEvents bmodifier As System.Windows.Forms.Button
    Friend WithEvents lrole As System.Windows.Forms.Label
    Friend WithEvents dgvrole As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodeSpec As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LibR As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

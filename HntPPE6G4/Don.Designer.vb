﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Don
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Don))
        Me.bnouveau = New System.Windows.Forms.Button()
        Me.ldon = New System.Windows.Forms.Label()
        Me.dgvdon = New System.Windows.Forms.DataGridView()
        Me.NumP = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Montant = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DateDon = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.bmenu = New System.Windows.Forms.Button()
        CType(Me.dgvdon, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'bnouveau
        '
        Me.bnouveau.BackColor = System.Drawing.Color.White
        Me.bnouveau.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bnouveau.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bnouveau.Location = New System.Drawing.Point(12, 273)
        Me.bnouveau.Name = "bnouveau"
        Me.bnouveau.Size = New System.Drawing.Size(195, 58)
        Me.bnouveau.TabIndex = 15
        Me.bnouveau.Text = "NOUVEAU DON"
        Me.bnouveau.UseVisualStyleBackColor = False
        '
        'ldon
        '
        Me.ldon.AutoSize = True
        Me.ldon.BackColor = System.Drawing.Color.White
        Me.ldon.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ldon.Location = New System.Drawing.Point(12, 9)
        Me.ldon.Name = "ldon"
        Me.ldon.Size = New System.Drawing.Size(75, 31)
        Me.ldon.TabIndex = 12
        Me.ldon.Text = "DON"
        '
        'dgvdon
        '
        Me.dgvdon.AllowUserToAddRows = False
        Me.dgvdon.AllowUserToDeleteRows = False
        Me.dgvdon.AllowUserToResizeColumns = False
        Me.dgvdon.AllowUserToResizeRows = False
        Me.dgvdon.BackgroundColor = System.Drawing.Color.White
        Me.dgvdon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvdon.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NumP, Me.Montant, Me.DateDon})
        Me.dgvdon.Location = New System.Drawing.Point(12, 63)
        Me.dgvdon.Name = "dgvdon"
        Me.dgvdon.Size = New System.Drawing.Size(343, 184)
        Me.dgvdon.TabIndex = 11
        '
        'NumP
        '
        Me.NumP.Frozen = True
        Me.NumP.HeaderText = "Num Donateur"
        Me.NumP.Name = "NumP"
        Me.NumP.ReadOnly = True
        '
        'Montant
        '
        Me.Montant.Frozen = True
        Me.Montant.HeaderText = "Montant"
        Me.Montant.Name = "Montant"
        Me.Montant.ReadOnly = True
        '
        'DateDon
        '
        Me.DateDon.Frozen = True
        Me.DateDon.HeaderText = "DateDon"
        Me.DateDon.Name = "DateDon"
        Me.DateDon.ReadOnly = True
        '
        'bmenu
        '
        Me.bmenu.BackColor = System.Drawing.Color.White
        Me.bmenu.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmenu.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.bmenu.Location = New System.Drawing.Point(220, 284)
        Me.bmenu.Name = "bmenu"
        Me.bmenu.Size = New System.Drawing.Size(142, 47)
        Me.bmenu.TabIndex = 16
        Me.bmenu.Text = "Menu"
        Me.bmenu.UseVisualStyleBackColor = False
        '
        'Don
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(374, 343)
        Me.Controls.Add(Me.bnouveau)
        Me.Controls.Add(Me.ldon)
        Me.Controls.Add(Me.dgvdon)
        Me.Controls.Add(Me.bmenu)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Don"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Don"
        CType(Me.dgvdon, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bnouveau As System.Windows.Forms.Button
    Friend WithEvents ldon As System.Windows.Forms.Label
    Friend WithEvents dgvdon As System.Windows.Forms.DataGridView
    Friend WithEvents bmenu As System.Windows.Forms.Button
    Friend WithEvents NumP As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Montant As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DateDon As System.Windows.Forms.DataGridViewTextBoxColumn
End Class

﻿Public Class Role
    Dim maconnexion As New Connexion
    Private Sub Role_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_role()
        bmodifier.Visible = False
        bsupprimer.Visible = False
    End Sub
    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim CodeSpec As String
        Dim IdLigne As String
        With dgvrole
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(2).Value
            CodeSpec = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Role_Modifier.tbcoder.Text = IdLigne
            Role_Modifier.tblibrole.Text = ValLigne
            Role_Modifier.cbcodespec.SelectedValue = CodeSpec
        End With
        Me.Close()
        Role_Modifier.Show()
    End Sub

    Private Sub bnouveau_Click(sender As Object, e As EventArgs) Handles bnouveau.Click
        Me.Close()
        Role_Ajouter.Show()
    End Sub

    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub

    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvrole
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(2).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer le rôle : " + IdLigne + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_role(ValLigne)
            End If
        End With
        Me.Close()
        Role_Chargement.Show()
    End Sub

    Private Sub dgvrole_CellMouseClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvrole.CellMouseClick
        bmodifier.Visible = True
        bsupprimer.Visible = True
    End Sub
End Class
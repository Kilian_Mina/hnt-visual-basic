﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Pays_Ajouter_Vaccin
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Pays_Ajouter_Vaccin))
        Me.bajout = New System.Windows.Forms.Button()
        Me.cbvaccin = New System.Windows.Forms.ComboBox()
        Me.lnommembre = New System.Windows.Forms.Label()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.lmissionajoutmembre = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'bajout
        '
        Me.bajout.BackColor = System.Drawing.Color.White
        Me.bajout.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bajout.Location = New System.Drawing.Point(54, 161)
        Me.bajout.Name = "bajout"
        Me.bajout.Size = New System.Drawing.Size(88, 35)
        Me.bajout.TabIndex = 42
        Me.bajout.Text = "AJOUTER"
        Me.bajout.UseVisualStyleBackColor = False
        '
        'cbvaccin
        '
        Me.cbvaccin.FormattingEnabled = True
        Me.cbvaccin.Location = New System.Drawing.Point(177, 101)
        Me.cbvaccin.Name = "cbvaccin"
        Me.cbvaccin.Size = New System.Drawing.Size(121, 21)
        Me.cbvaccin.TabIndex = 39
        '
        'lnommembre
        '
        Me.lnommembre.AutoSize = True
        Me.lnommembre.BackColor = System.Drawing.Color.White
        Me.lnommembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lnommembre.Location = New System.Drawing.Point(15, 102)
        Me.lnommembre.Name = "lnommembre"
        Me.lnommembre.Size = New System.Drawing.Size(127, 17)
        Me.lnommembre.TabIndex = 38
        Me.lnommembre.Text = "Description vaccin:"
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(177, 65)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 37
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(15, 65)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 36
        Me.lid.Text = "ID : "
        '
        'lmissionajoutmembre
        '
        Me.lmissionajoutmembre.AutoSize = True
        Me.lmissionajoutmembre.BackColor = System.Drawing.Color.White
        Me.lmissionajoutmembre.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lmissionajoutmembre.Location = New System.Drawing.Point(12, 9)
        Me.lmissionajoutmembre.Name = "lmissionajoutmembre"
        Me.lmissionajoutmembre.Size = New System.Drawing.Size(353, 31)
        Me.lmissionajoutmembre.TabIndex = 35
        Me.lmissionajoutmembre.Text = "Ajouter un Vaccin à un Pays"
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(285, 173)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Pays_Ajouter_Vaccin
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(372, 208)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.bajout)
        Me.Controls.Add(Me.cbvaccin)
        Me.Controls.Add(Me.lnommembre)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.lmissionajoutmembre)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Pays_Ajouter_Vaccin"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Pays_Ajouter_Vaccin"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents bajout As System.Windows.Forms.Button
    Friend WithEvents cbvaccin As System.Windows.Forms.ComboBox
    Friend WithEvents lnommembre As System.Windows.Forms.Label
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents lmissionajoutmembre As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class

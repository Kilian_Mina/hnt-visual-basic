﻿Public Class Don_Ajouter
    Dim undon As New Connexion
    Private Sub bvalider_Click(sender As Object, e As EventArgs) Handles bvalider.Click

        undon.ajouter_don(donateur:=cbdon.SelectedItem, montant:=montant.Value, datedon:=datedon.Value)
        Me.Hide()
        Don.Show()
    End Sub

    Private Sub Don_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        undon.remplir_donateur_don()
    End Sub


    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Don.Show()
    End Sub
End Class
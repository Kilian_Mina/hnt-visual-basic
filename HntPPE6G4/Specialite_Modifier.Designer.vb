﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Specialite_Modifier
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Specialite_Modifier))
        Me.tblibspec = New System.Windows.Forms.TextBox()
        Me.llibspec = New System.Windows.Forms.Label()
        Me.bmodif = New System.Windows.Forms.Button()
        Me.lspecmodif = New System.Windows.Forms.Label()
        Me.tbid = New System.Windows.Forms.TextBox()
        Me.lid = New System.Windows.Forms.Label()
        Me.bretour = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'tblibspec
        '
        Me.tblibspec.Location = New System.Drawing.Point(130, 100)
        Me.tblibspec.Name = "tblibspec"
        Me.tblibspec.Size = New System.Drawing.Size(100, 20)
        Me.tblibspec.TabIndex = 33
        '
        'llibspec
        '
        Me.llibspec.AutoSize = True
        Me.llibspec.BackColor = System.Drawing.Color.White
        Me.llibspec.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.llibspec.Location = New System.Drawing.Point(12, 101)
        Me.llibspec.Name = "llibspec"
        Me.llibspec.Size = New System.Drawing.Size(57, 17)
        Me.llibspec.TabIndex = 32
        Me.llibspec.Text = "Libellé :"
        '
        'bmodif
        '
        Me.bmodif.BackColor = System.Drawing.Color.White
        Me.bmodif.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bmodif.Location = New System.Drawing.Point(15, 152)
        Me.bmodif.Name = "bmodif"
        Me.bmodif.Size = New System.Drawing.Size(118, 30)
        Me.bmodif.TabIndex = 31
        Me.bmodif.Text = "Modifier"
        Me.bmodif.UseVisualStyleBackColor = False
        '
        'lspecmodif
        '
        Me.lspecmodif.AutoSize = True
        Me.lspecmodif.BackColor = System.Drawing.Color.White
        Me.lspecmodif.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lspecmodif.Location = New System.Drawing.Point(12, 9)
        Me.lspecmodif.Name = "lspecmodif"
        Me.lspecmodif.Size = New System.Drawing.Size(272, 31)
        Me.lspecmodif.TabIndex = 28
        Me.lspecmodif.Text = "Modifier un Spécialité"
        '
        'tbid
        '
        Me.tbid.Enabled = False
        Me.tbid.Location = New System.Drawing.Point(130, 61)
        Me.tbid.Name = "tbid"
        Me.tbid.Size = New System.Drawing.Size(100, 20)
        Me.tbid.TabIndex = 35
        '
        'lid
        '
        Me.lid.AutoSize = True
        Me.lid.BackColor = System.Drawing.Color.White
        Me.lid.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lid.Location = New System.Drawing.Point(12, 64)
        Me.lid.Name = "lid"
        Me.lid.Size = New System.Drawing.Size(33, 17)
        Me.lid.TabIndex = 34
        Me.lid.Text = "ID : "
        '
        'bretour
        '
        Me.bretour.BackColor = System.Drawing.Color.White
        Me.bretour.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.bretour.Location = New System.Drawing.Point(198, 159)
        Me.bretour.Name = "bretour"
        Me.bretour.Size = New System.Drawing.Size(75, 23)
        Me.bretour.TabIndex = 47
        Me.bretour.Text = "RETOUR"
        Me.bretour.UseVisualStyleBackColor = False
        '
        'Specialite_Modifier
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackgroundImage = CType(resources.GetObject("$this.BackgroundImage"), System.Drawing.Image)
        Me.ClientSize = New System.Drawing.Size(285, 194)
        Me.Controls.Add(Me.bretour)
        Me.Controls.Add(Me.tbid)
        Me.Controls.Add(Me.lid)
        Me.Controls.Add(Me.tblibspec)
        Me.Controls.Add(Me.llibspec)
        Me.Controls.Add(Me.bmodif)
        Me.Controls.Add(Me.lspecmodif)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Specialite_Modifier"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Specialite_Modifier"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents tblibspec As System.Windows.Forms.TextBox
    Friend WithEvents llibspec As System.Windows.Forms.Label
    Friend WithEvents bmodif As System.Windows.Forms.Button
    Friend WithEvents lspecmodif As System.Windows.Forms.Label
    Friend WithEvents tbid As System.Windows.Forms.TextBox
    Friend WithEvents lid As System.Windows.Forms.Label
    Friend WithEvents bretour As Button
End Class

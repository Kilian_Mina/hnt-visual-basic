﻿Public Class Pays
    Dim maconnexion As New Connexion
    Private Sub Pays_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_pays()
        bmodifier.Visible = False
        bsupprimer.Visible = False
        bajoutvac.Visible = False
        btsupmembre.Visible = False
    End Sub

    Private Sub bmodifier_Click(sender As Object, e As EventArgs) Handles bmodifier.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        Dim Risque As String
        With dgvpays
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Risque = .Rows(NumLigne).Cells(2).Value
            Pays_Modifier.tbid.Text = IdLigne
            Pays_Modifier.tbnom.Text = ValLigne
            If Pays_Modifier.rbaucun.Text = Risque Then
                Pays_Modifier.rbaucun.Checked = True
            End If
            If Pays_Modifier.rbfaible.Text = Risque Then
                Pays_Modifier.rbfaible.Checked = True
            End If
            If Pays_Modifier.rbmoyen.Text = Risque Then
                Pays_Modifier.rbmoyen.Checked = True
            End If
            If Pays_Modifier.rbeleve.Text = Risque Then
                Pays_Modifier.rbeleve.Checked = True
            End If
            If Pays_Modifier.rbtreseleve.Text = Risque Then
                Pays_Modifier.rbtreseleve.Checked = True
            End If
        End With
        Me.Close()
        Pays_Modifier.Show()
    End Sub

    Private Sub bnouveau_Click(sender As Object, e As EventArgs) Handles bnouveau.Click
        Me.Close()
        Pays_Ajouter.Show()
    End Sub

    Private Sub bmenu_Click(sender As Object, e As EventArgs) Handles bmenu.Click
        Me.Close()
        Menu_app.Show()
    End Sub
    Private Sub bsupprimer_Click(sender As Object, e As EventArgs) Handles bsupprimer.Click
        Dim NumLigne As Integer
        Dim ValLigne As String
        Dim IdLigne As String
        With dgvpays
            NumLigne = .CurrentCell.RowIndex
            ValLigne = .Rows(NumLigne).Cells(1).Value
            IdLigne = .Rows(NumLigne).Cells(0).Value
            If MsgBox("Etes vous sur de vouloir supprimer le pays : " + IdLigne + " ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_pays(ValLigne)
            End If
        End With
        Me.Close()
        Pays_Chargement.Show()
    End Sub

    Private Sub btsupmembre_Click(sender As Object, e As EventArgs) Handles btsupmembre.Click
        Dim NumLigne As Integer
        Dim ValId As String
        Dim ValIDvaccin As String
        With lvvaccin
            NumLigne = .SelectedIndices(0)
            ValId = .Items(NumLigne).SubItems(0).Text
            ValIDvaccin = .Items(NumLigne).SubItems(1).Text
            If MsgBox("Etes vous sur de vouloir retirer le vaccin : " + ValIDvaccin + " de ce pays ?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                maconnexion.supprimer_vaccin_pays(NPays:=ValId, NumV:=ValIDvaccin)
            End If
        End With
        Me.Close()
        Pays_Chargement.Show()
    End Sub

    Private Sub bajoutvac_Click(sender As Object, e As EventArgs) Handles bajoutvac.Click
        Dim NumLigne As Integer
        Dim IdLigne As String
        With dgvpays
            NumLigne = .CurrentCell.RowIndex
            IdLigne = .Rows(NumLigne).Cells(0).Value
            Pays_Ajouter_Vaccin.tbid.Text = IdLigne
        End With
        Me.Close()
        Pays_Ajouter_Vaccin.Show()
    End Sub

    Private Sub dgvpays_CellMouseClick1(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgvpays.CellMouseClick
        bsupprimer.Visible = True
        bmodifier.Visible = True
        bajoutvac.Visible = True
        Me.lvvaccin.Items.Clear()
        Dim NumLigne As Integer
        Dim ValLigne1 As String
        With dgvpays
            NumLigne = .CurrentCell.RowIndex
            ValLigne1 = .Rows(NumLigne).Cells(0).Value
        End With
        maconnexion.remplir_liste_vaccin(idpays:=ValLigne1)
    End Sub

    Private Sub lvvaccin_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lvvaccin.SelectedIndexChanged
        btsupmembre.Visible = True
    End Sub

    Private Sub dgvpays_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvpays.CellContentClick

    End Sub
End Class
﻿Public Class Mission_Ajouter
    Dim maconnexion As New Connexion
    Private Sub bajout_Click(sender As Object, e As EventArgs) Handles bajout.Click
        maconnexion.ajouter_mission(
            numpays:=cbnompays.SelectedItem,
            nom:=tbnommission.Text,
            description:=tbdescrip.Text,
            datecrea:=datecreat.Value.ToShortDateString(),
            datedeb:=datedeb.Value.ToShortDateString(),
            datefin:=datefin.Value.ToShortDateString())
        Me.Hide()
        Mission.Show()
    End Sub

    Private Sub Mission_Ajouter_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        maconnexion.remplir_mission_pays()
        tbdescrip.Text = ""
        tbnommission.Text = ""
    End Sub

    Private Sub bretour_Click(sender As Object, e As EventArgs) Handles bretour.Click
        Me.Close()
        Mission.Show()
    End Sub
End Class